from flask_wtf import FlaskForm
from wtforms import StringField, FloatField, SelectField, SubmitField
from wtforms.validators import DataRequired, Length

CATEGORY_CHOICES = (
    ("Fruits", "Fruits"),
    ("Drinks", "Drinks"),
    ("Clothes", "Clothes"),
)

class AddProductForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=30)])
    price = FloatField('Price', validators=[DataRequired()])
    unit = StringField('Unit', validators=[DataRequired()])
    category = SelectField('Category', choices=CATEGORY_CHOICES)
    submit = SubmitField('Add Product')