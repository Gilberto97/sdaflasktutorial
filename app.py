from flask import Flask, render_template, url_for, redirect
from forms import AddProductForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'as8908as7dfj2nASD213f'

products = [
    {
        'name': 'Apple',
        'price': 2.50,
        'unit': 'kg',
        'category': 'Fruit'
    },
    {
        'name': 'Orange',
        'price': 3.00,
        'unit': 'kg',
        'category': 'Fruit'
    },
    {
        'name': 'Banana',
        'price': 4.00,
        'unit': 'kg',
        'category': 'Fruit'
    },
]

@app.route('/home')
@app.route('/')
def index():
    return render_template('index.html', products=products, title="Main")

@app.route('/addproduct', methods=["GET","POST"])
def add_product():
    form = AddProductForm()
    if form.validate_on_submit():
        products.append({
            'name': form.name.data,
            'price': form.price.data,
            'unit': form.unit.data,
            'category': form.category.data
        })
        return redirect(url_for('index'))
    print(form.errors)

    return render_template('add_product.html', title="Add Product", form=form)

@app.route('/about')
def about():
    return render_template('about.html', title="About")

if __name__ == "__main__":
    app.run(debug=True)